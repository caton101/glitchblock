// define header
#ifndef BLOCK_H
#define BLOCK_H

// inform compiler of needed libs
#include <SDL2/SDL.h>
#include <string>
#include <list>
#include <random>

// inform compiler of needed globals (defined in main.cpp)
extern int SCREEN_HEIGHT;
extern int SCREEN_WIDTH;
extern double BLOCK_SPEED;
extern double BLOCK_TIMER;
extern double BLOCK_SPAWN_DELAY;
extern double CLOCK_DELTA;
extern double BLOCK_OFFSET;
extern double BLOCK_WIDTH;
extern double BLOCK_HEIGHT;

// define some contants
#define Y_AXIS true
#define X_AXIS false

// declare class for blocks
class Block {
    public:
        double x;
        double y;
        double width;
        double height;
        double offset;
        bool active;
        bool scoreClaimed;
        bool showHitbox;
        SDL_Renderer *rend;
        SDL_Rect redRect;
        SDL_Rect greenRect;
        SDL_Rect blueRect;
        Block();
        Block(double, double, double, double, double, SDL_Renderer *);
        double __calcOffset__(bool);
        double __calcBounds__(double, bool);
        std::string status();
        void render();
        void transformX(double);
        void transformY(double);
        void changeWidth(double);
        void changeHeight(double);
        void changeOffset(double);
        void toggleActive();
        void toggleHitbox();
        bool claimScore();
        bool shouldDie();
};
// declare helper functions related to the block class
void spawnBlock(std::list <Block> *blocks, SDL_Renderer *rend);
void updateBlocks(std::list <Block> *blocks, SDL_Renderer *rend);

// end header
#endif