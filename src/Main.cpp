// import needed file
#include "Main.h"

/**
 * This function sets the runtime variables.
 */
void init() {
    CLOCK_OLD = getTime();
    CLOCK_NEW = getTime();
    CLOCK_DELTA = 0;
    BLOCK_TIMER = 0;
    GAME_STATE = 0;
}

/**
 * This function exits the game
 * 
 * @param w the SDL_Window used by the renderer
 * @param r the SDL_Renderer used to draw on the window
 * @returns nothing
 */
void stop(SDL_Window *w, SDL_Renderer *r) {
    SDL_DestroyRenderer(r);
    SDL_DestroyWindow(w);
    exit(0);
}

/**
 * This function calls chronos in order to pull the system time in nanoseconds since the Epoch
 * 
 * @returns the amount of nanoseconds since the Epoch
 */
nanoseconds getTime() {
    return duration_cast<nanoseconds>(system_clock::now().time_since_epoch());
}

/**
 * This function performs some basic math to save the amount of seconds the game took to render the last frame.
 * 
 * NOTE: the result of this operation is stored in the CLOCK_DELTA global variable
 */
void calcDeltaTime() {
    CLOCK_OLD = CLOCK_NEW;
    CLOCK_NEW = getTime();
    CLOCK_DELTA = (CLOCK_NEW - CLOCK_OLD).count()*0.000000001;
}

/**
 * This contains all initialization and control logic for the game.
 * 
 * @param argc not used
 * @param argv not used
 */
int main() {
    // set runtime variables
    init();
    
    // declare variables for main
    SDL_Event event;
    std::list <Block> blocks;

    // initialize SDL
    SDL_Init(SDL_INIT_VIDEO);
    // create the window
    SDL_Window * window = SDL_CreateWindow(
        "Glitchblock",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH, SCREEN_HEIGHT,
        SDL_WINDOW_RESIZABLE
    );
    // create the renderer for drawing
    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    // seed the random number generator with the current time
    srand(getTime().count());

    // make first block for player to stand
    //                      x         y        w       h             o           r
    blocks.push_front(Block(0, 1-BLOCK_HEIGHT, 1, BLOCK_HEIGHT, BLOCK_OFFSET, renderer));
    // make the player
    //                      x    y     s     g         o           b        r
    Player player = Player(0.5, 0.5, 0.005, true, BLOCK_OFFSET, &blocks, renderer);
    
    // enter main loop
    while (true) {
        // get events
        SDL_PollEvent(&event);
        // game states will effect what happens during frame updates
        switch (GAME_STATE) {
            case 0:
                // wait for user input before starting the game
                {
                    // process event queue
                    switch (event.type) {
                        case SDL_QUIT:
                            // close the game
                            GAME_STATE = 3;
                            break;
                        case SDL_WINDOWEVENT:
                            if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                                // update globals with new window size
                                SCREEN_WIDTH = event.window.data1;
                                SCREEN_HEIGHT = event.window.data2;
                                // do not render game events, just start over
                                continue;
                            }
                            break;
                        case SDL_KEYDOWN:
                            switch(event.key.keysym.scancode) {
                                case SDL_SCANCODE_SPACE:
                                    // start the game
                                    GAME_STATE = 1;
                                    break;
                                case SDL_SCANCODE_ESCAPE:
                                case SDL_SCANCODE_Q:
                                    // game needs to exit
                                    GAME_STATE = 3;
                                    break;
                                default:
                                    // do not process an unknown event
                                    break;
                            }
                            break;
                        default:
                            // do not process an unknown event
                            break;
                    }
                    // render screen for paused state
                    pauseScreen(renderer);
                    // end case for game state 0
                }
                break;
            case 1:
                // the player is alive
                {
                   // process event queue
                    switch (event.type) {
                        case SDL_QUIT:
                            // close the game
                            GAME_STATE = 3;
                            break;
                        case SDL_WINDOWEVENT:
                            if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                                // update globals with new window size
                                SCREEN_WIDTH = event.window.data1;
                                SCREEN_HEIGHT = event.window.data2;
                                // do not render game events, just start over
                                continue;
                            }
                            break;
                        case SDL_KEYDOWN:
                            switch(event.key.keysym.scancode) {
                                case SDL_SCANCODE_O:
                                    // toggle glitch effect for blocks
                                    if (DEBUG_KEYS) {
                                        for (auto &i : blocks) {
                                            i.toggleHitbox();
                                        }
                                    }
                                    break;
                                case SDL_SCANCODE_P:
                                    // toggle glitch effect for blocks
                                    if (DEBUG_KEYS) {
                                        for (auto &i : blocks) {
                                            i.toggleActive();
                                        }
                                    }
                                    break;
                                case SDL_SCANCODE_M:
                                    // show debug info for all blocks
                                    if (DEBUG_KEYS) {
                                        for (auto &i : blocks) {
                                            printf("%s\n",i.status().c_str());
                                        }
                                    }
                                    break;
                                case SDL_SCANCODE_E:
                                    // show player hitbox
                                    if (DEBUG_KEYS) {
                                        player.toggleHitbox();
                                    }
                                    break;
                                case SDL_SCANCODE_R:
                                    // toggle glitch effect for player
                                    if (DEBUG_KEYS) {
                                        player.toggleGlitched();
                                    }
                                    break;
                                case SDL_SCANCODE_Z:
                                    // show debug info for player
                                    if (DEBUG_KEYS) {
                                        printf("%s\n",player.status().c_str());
                                    }
                                    break;
                                case SDL_SCANCODE_9:
                                    // increment player score
                                    if (DEBUG_KEYS) {
                                        player.updateScore(1);
                                    }
                                    break;
                                case SDL_SCANCODE_0:
                                    // decrement player score
                                    if (DEBUG_KEYS) {
                                        player.updateScore(-1);
                                    }
                                    break;
                                case SDL_SCANCODE_SPACE:
                                    // process player jump
                                    player.jump();
                                    break;
                                case SDL_SCANCODE_Y:
                                    // yield until done
                                    if (DEBUG_KEYS) {
                                        GAME_STATE = 4;
                                    }
                                    break;
                                case SDL_SCANCODE_GRAVE:
                                    // toggle debug keys
                                    DEBUG_KEYS = !DEBUG_KEYS;
                                    break;
                                case SDL_SCANCODE_ESCAPE:
                                case SDL_SCANCODE_Q:
                                    // game needs to exit
                                    GAME_STATE = 3;
                                    break;
                                default:
                                    // do not process an unknown key
                                    break;
                            }
                            break;
                        default:
                            // do not process an unknown event
                            break;
                    }
                    // get keyboard data
                    const Uint8 *keyboard = SDL_GetKeyboardState(NULL);
                    // check for player moving up
                    if (keyboard[SDL_SCANCODE_W] && DEBUG_KEYS) {
                        player.transformY(-PLAYER_SPEED * CLOCK_DELTA);
                    }
                    // check for player moving down
                    if (keyboard[SDL_SCANCODE_S] && DEBUG_KEYS) {
                        player.transformY(PLAYER_SPEED * CLOCK_DELTA);
                    }
                    // check for player moving left
                    if (keyboard[SDL_SCANCODE_A]) {
                        player.transformX(-PLAYER_SPEED * CLOCK_DELTA);
                    }
                    // check for player moving right
                    if (keyboard[SDL_SCANCODE_D]) {
                        player.transformX(PLAYER_SPEED * CLOCK_DELTA);
                    }
                    // check for blocks moving up
                    if (keyboard[SDL_SCANCODE_I] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.transformY(-PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // check for blocks moving down
                    if (keyboard[SDL_SCANCODE_K] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.transformY(PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // check for blocks moving left
                    if (keyboard[SDL_SCANCODE_J] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.transformX(-PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // check for blocks moving right
                    if (keyboard[SDL_SCANCODE_L] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.transformX(PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // decrease block width
                    if (keyboard[SDL_SCANCODE_1] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.changeWidth(-PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // increase block width
                    if (keyboard[SDL_SCANCODE_2] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.changeWidth(PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // decrease block height
                    if (keyboard[SDL_SCANCODE_3] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.changeHeight(-PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // increase block height
                    if (keyboard[SDL_SCANCODE_4] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.changeHeight(PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // decrease block offset effect
                    if (keyboard[SDL_SCANCODE_5] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.changeOffset(-PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // increase block offset effect
                    if (keyboard[SDL_SCANCODE_6] && DEBUG_KEYS) {
                        for (auto &i : blocks) {
                            i.changeOffset(PLAYER_SPEED * CLOCK_DELTA);
                        }
                    }
                    // refresh player
                    player.update();
                    // check if player died
                    if (!player.isAlive) {
                        // change the game state to dead
                        GAME_STATE = 2;
                    }
                    updateBlocks(&blocks, renderer);
                    // render scene
                    renderGameScene(blocks, renderer, &player);
                    // end case for game state 1
                }
                break;
            case 2:
                // the player died
                {
                    // process event queue
                    switch (event.type) {
                        case SDL_QUIT:
                            // close the game
                            GAME_STATE = 3;
                            break;
                        case SDL_WINDOWEVENT:
                            if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                                // update globals with new window size
                                SCREEN_WIDTH = event.window.data1;
                                SCREEN_HEIGHT = event.window.data2;
                                // do not render game events, just start over
                                continue;
                            }
                            break;
                        case SDL_KEYDOWN:
                            switch(event.key.keysym.scancode) {
                                case SDL_SCANCODE_SPACE:
                                case SDL_SCANCODE_ESCAPE:
                                case SDL_SCANCODE_Q:
                                    // game needs to exit
                                    GAME_STATE = 3;
                                    break;
                                default:
                                    // do not process an unknown event
                                    break;
                            }
                            break;
                        default:
                            // do not process an unknown event
                            break;
                    }
                    // render screen for death
                    deathScreen(renderer);
                    // end case for game state 2
                }
                break;
            case 3:
                // the game should end
                // NOTE: the extra spaces remove the status string from other running states
                printf("Your final score is %d.                                                                         \n",player.score);
                stop(window, renderer);
                // end case for game state 3
            case 4:
                // the game is yielding to the player
                {
                    // process event queue
                    switch (event.type) {
                        case SDL_QUIT:
                            // close the game
                            GAME_STATE = 3;
                            break;
                        case SDL_WINDOWEVENT:
                            if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                                // update globals with new window size
                                SCREEN_WIDTH = event.window.data1;
                                SCREEN_HEIGHT = event.window.data2;
                                // do not render game events, just start over
                                continue;
                            }
                            break;
                        case SDL_KEYDOWN:
                            switch(event.key.keysym.scancode) {
                                case SDL_SCANCODE_SPACE:
                                case SDL_SCANCODE_ESCAPE:
                                case SDL_SCANCODE_Q:
                                    // game needs to exit
                                    GAME_STATE = 3;
                                    break;
                                case SDL_SCANCODE_Y:
                                    GAME_STATE = 1;
                                    break;
                                default:
                                    // do not process an unknown event
                                    break;
                            }
                            break;
                        default:
                            // do not process an unknown event
                            break;
                    }
                    // render scene
                    renderYieldScene(blocks, renderer, &player);
                    // end case for game state 4
                }
                break;
            default:
                // this state should never happen
                printf("\n=== CRITICAL ERROR ===\nGame reached invalid state \"%d\".\nExiting program to protect itself.\n======================\n",GAME_STATE);
                // exit game to prevent any issues
                GAME_STATE = 3;
                break;
        }
        // render clock delta
        calcDeltaTime();
        // process status
        printf("%8.8f sec.\tScore: %3d \tState: %d \tBlocks: %ld \tTimer: %f \tDebug: %d\r", CLOCK_DELTA, player.score, GAME_STATE, blocks.size(),BLOCK_TIMER, DEBUG_KEYS);
    }
}

/**
 * This is a massive keymap for everything this game does.
 *  ________________________________________________________________________________________
 * |       |       |       |       |       |       |       |       |       |       |       |
 * |       |       |       |       |       |       |       |       |       |       |       |
 * |   ~   |   1   |   2   |   3   |   4   |   5   |   6   |   7   |   8   |   9   |   0   |
 * | DEBUG | -WDTH | +WDTH | -HGHT | +HGHT |  -OFF |  +OFF |       |       |  -PNT |  +PNT |
 * |_______|_______|_______|_______|_______|_______|_______|_______|_______|_______|_______|__
 *           |       |       |       |       |       |       |       |       |       |       |
 *           |       |       |       |       |       |       |       |       |       |       |
 *           |   Q   |   W   |   E   |   R   |   T   |   Y   |   U   |   I   |   O   |   P   |
 *           |  QUIT |   UP  | HITBX |  ACT  |       | YIELD |       |   UP  | HITBX |  ACT  |
 *           |_______|_______|_______|_______|_______|_______|_______|_______|_______|_______|
 *              |       |       |       |       |       |       |       |       |       |
 *              |       |       |       |       |       |       |       |       |       |
 *              |   A   |   S   |   D   |   F   |   G   |   H   |   J   |   K   |   L   |
 *              |  LEFT |  DOWN | RIGHT |       |       |       |  LEFT |  DOWN | RIGHT |
 *              |_______|_______|_______|_______|_______|_______|_______|_______|_______|
 *                 |       |       |       |       |       |       |       |
 *                 |       |       |       |       |       |       |       |
 *                 |   Z   |   X   |   C   |   V   |   B   |   N   |   M   |
 *                 |  INFO |       |       |       |       |       |  INFO |
 *                 |_______|_______|_______|_______|_______|_______|_______|____
 *                     |                                                       |
 *                     |                                                       |
 *                     |                         SPACE                         |
 *                     |                          jump                         |
 *                     |_______________________________________________________|
 */
